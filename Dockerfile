FROM node:21

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

ENV PORT=8080
ENV MONGO_URI=mongodb://localhost:27017/humidor
ENV API_URL=http://localhost/raspberrypiapi

EXPOSE 8080

CMD [ "node", "index.js" ]