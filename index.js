const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const axios = require('axios');
const schedule = require('node-schedule');
require('dotenv').config();

mongoose
  .connect(process.env.MONGO_URI)
  .then(() => console.log('MongoDB connected successfully.'))
  .catch((err) => console.error('MongoDB connection error:', err));

const app = express();
app.use(express.static('public'));
app.use(cors());
app.use(express.json());

const weatherSchema = new mongoose.Schema({
  humidityPerc: { type: Number, required: true },
  tempFahrenheit: { type: Number, required: true }
}, { 
  timestamps: { createdAt: true, updatedAt: false } 
});

weatherSchema.index({ 'createdAt': -1 });

const Weather = mongoose.model('Weather', weatherSchema);

Weather.syncIndexes()
  .then(() => console.log('Indexes are synchronized'))
  .catch(err => console.error('Error synchronizing indexes:', err));

let lastValue = {}

async function fetchData() {
  try {
    const { data } = await axios.get(process.env.API_URL);
    if (lastValue.humidityPerc == data.humidityPerc && lastValue.tempFahrenheit == data.tempFahrenheit)
      return;
    lastValue = data;
    const weatherData = await Weather.create({
      humidityPerc: parseFloat(data.humidityPerc),
      tempFahrenheit: parseFloat(data.tempFahrenheit)
    });
    console.log('Data saved:', weatherData);
  } catch (error) {
    console.error('Error fetching data:', error);
  }
}

schedule.scheduleJob('*/10 * * * * *', fetchData);

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

app.get('/api/humidor/last24hours', async (req, res, next) => {
  const endDate = new Date();
  const startDate = new Date(endDate.getTime() - 24 * 60 * 60 * 1000);
  try {
    const weatherData = await Weather.find({
      createdAt: { $gte: startDate, $lte: endDate }
    });
    res.json(weatherData);
  } catch (error) {
    next(error);
  }
});

app.get('/api/humidor', async (req, res, next) => {
  const { start, end } = req.query;
  if (!start || !end) {
    return res.status(400).send('Start and end dates are required');
  }
  try {
    const weatherData = await Weather.find({
      createdAt: { $gte: new Date(start), $lte: new Date(end) }
    });
    res.json(weatherData);
  } catch (error) {
    next(error)
  }
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
