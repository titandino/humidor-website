document.addEventListener('DOMContentLoaded', function () {
  Highcharts.setOptions({
    global: {
      timezone: 'America/Los_Angeles', // Automatically adjust for PST/PDT
    },
    colors: ['#f54242', '#4287f5'],
    chart: {
      backgroundColor: 'rgba(139, 69, 19, 0.5)',
      style: {
        fontFamily: 'Arial, sans-serif',
      },
      borderColor: '#221B11',
      borderWidth: 2,
    },
    title: {
      style: {
        color: '#F5F5DC',
      },
    },
    xAxis: {
      type: 'datetime',
      labels: {
        style: {
          color: '#F5F5DC',
        },
      },
      dateTimeLabelFormats: {
        hour: '%I %p',
        minute: '%I:%M %p'
      },
    },
    yAxis: {
      labels: {
        style: {
          color: '#F5F5DC',
        },
      },
    },
    legend: {
      itemStyle: {
        color: '#F5F5DC',
      },
      backgroundColor: 'rgba(139, 69, 19, 0.8)',
      borderColor: '#221B11',
    },
    tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
        color: '#F5F5DC',
      },
    },
  });

  const chart = Highcharts.chart('humidorChart', {
    chart: {
      type: 'spline',
    },
    title: {
      text: 'Last 24 Hours',
    },
    xAxis: {
      type: 'datetime',
      title: {
        text: 'Time',
      },
    },
    yAxis: [
      {
        title: {
          text: 'Temperature (°F)',
        },
        labels: {
          format: '{value} °F',
        },
        min: 30,
        max: 90
      },
      {
        title: {
          text: 'Humidity (%)',
        },
        labels: {
          format: '{value} %',
        },
        opposite: true,
        min: 30,
        max: 90,
      },
    ],
    tooltip: {
      shared: true,
    },
    legend: {
      layout: 'vertical',
      align: 'left',
      x: 75,
      verticalAlign: 'top',
      y: 275,
      floating: true,
    },
    series: [
      {
        name: 'Temperature',
        data: [],
        yAxis: 0,
        tooltip: {
          valueSuffix: ' °F',
        },
      },
      {
        name: 'Humidity',
        data: [],
        yAxis: 1,
        tooltip: {
          valueSuffix: ' %',
        },
      },
    ],
  });

  document.getElementById('loadData').addEventListener('click', function () {
    const startDateTime = document.getElementById('startDateTime').value;
    const endDateTime = document.getElementById('endDateTime').value;
    if (startDateTime && endDateTime) {
      chart.cl
      fetchChartData(chart, startDateTime, endDateTime);
    } else {
      alert('Please select both start and end date times.');
    }
  });
  fetchChartData(chart);
});

async function fetchChartData(chart, start, end) {
  try {
    const response = await fetch((!start || !end) ? '/api/humidor/last24hours' : `/api/humidor?start=${encodeURIComponent(start)}&end=${encodeURIComponent(end)}`);
    const weatherData = await response.json();

    let tempData = [];
    let humData = [];

    weatherData.forEach((data) => {
      const utcTime = new Date(data.createdAt);
      const pstTime = utcTime.toLocaleString('en-US', { timeZone: 'America/Los_Angeles' });
      const time = new Date(pstTime).getTime();

      tempData.push([time, data.tempFahrenheit]);
      humData.push([time, data.humidityPerc]);
    });
    chart.series.forEach(function(series) {
      series.setData([]);
    });
    chart.series[0].setData(tempData);
    chart.series[1].setData(humData);
  } catch (error) {
    console.error('Failed to fetch data:', error);
  }
}