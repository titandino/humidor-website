#!/bin/bash

# Extract version number from package.json
VERSION=$(grep '"version":' package.json | cut -d '"' -f 4)

# Check if version is extracted correctly
if [ -z "$VERSION" ]; then
    echo "Error: Unable to extract version from package.json"
    exit 1
fi

# Build, tag, and push the Docker image with the extracted version
sudo docker build -t titandino/humidor-website:$VERSION .
sudo docker tag titandino/humidor-website:$VERSION registry.gitlab.com/titandino/humidor-website/humidor-website:$VERSION
sudo docker tag titandino/humidor-website:$VERSION registry.gitlab.com/titandino/humidor-website/humidor-website:latest
sudo docker push registry.gitlab.com/titandino/humidor-website/humidor-website:$VERSION
sudo docker push registry.gitlab.com/titandino/humidor-website/humidor-website:latest